const { AMQP_HOST, AMQP_PROTO = 'amqp', AMQP_PORT, AMQP_USER, AMQP_PASS } = process.env

export const { RMQ_EXCHANGE, RMQ_EXCHANGE_TYPE, RMQ_ROUTING_KEY } = process.env

export const RABBITMQ_URL = `${AMQP_PROTO}://${AMQP_USER}:${AMQP_PASS}@${AMQP_HOST}:${AMQP_PORT}`
