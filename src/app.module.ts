import { RabbitMQModule } from '@golevelup/nestjs-rabbitmq'
import { RABBITMQ_URL, RMQ_EXCHANGE, RMQ_EXCHANGE_TYPE } from '@lib/rabbitmq.constants'
import { Module } from '@nestjs/common'
import { AppService } from '@lib/app.service'

@Module({
  imports: [
    RabbitMQModule.forRoot(RabbitMQModule, {
      exchanges: [
        {
          name: RMQ_EXCHANGE,
          type: RMQ_EXCHANGE_TYPE,
        },
      ],
      uri: RABBITMQ_URL,
    }),
  ],
  providers: [
    AppService,
  ],
})
export class AppModule { }
