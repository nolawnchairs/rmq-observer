
import { ConsumeMessage } from 'amqplib'
import { Injectable, Logger } from '@nestjs/common'
import { RabbitSubscribe } from '@golevelup/nestjs-rabbitmq'
import { RMQ_EXCHANGE, RMQ_ROUTING_KEY } from '@lib/rabbitmq.constants'

@Injectable()
export class AppService {

  private readonly logger = new Logger(AppService.name)

  @RabbitSubscribe({
    exchange: RMQ_EXCHANGE,
    routingKey: RMQ_ROUTING_KEY,
    createQueueIfNotExists: true,
    queueOptions: {
      durable: false,
      autoDelete: true,
      exclusive: true,
    },
  })
  async onMessage(payload: object, context: ConsumeMessage) {
    this.logger.debug(`Message received (${context.content.length} bytes)`)
    this.logger.log(JSON.stringify(payload))
  }
}
