import { NestFactory } from '@nestjs/core'
import { AppModule } from '@lib/app.module'

async function bootstrap() {
  await NestFactory.createApplicationContext(AppModule)
}
bootstrap()
