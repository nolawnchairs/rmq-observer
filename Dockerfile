FROM node:14.16.1-alpine3.13

RUN echo 'UTC' > /etc/timezone
RUN mkdir -p /app && touch /app/.env
RUN npm install -g @nestjs/cli

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY *.json ./
COPY ./src ./src

RUN nest build

ENV LOG_DIR /logs

CMD ["nest", "start"]
